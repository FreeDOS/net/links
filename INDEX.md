# Links

Links is a Lynx-like text and graphics WWW browser.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## LINKS.LSM

<table>
<tr><td>title</td><td>Links</td></tr>
<tr><td>version</td><td>2.30</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-07-08</td></tr>
<tr><td>description</td><td>A Lynx-like text and graphics WWW browser</td></tr>
<tr><td>summary</td><td>Links is a Lynx-like text and graphics WWW browser.</td></tr>
<tr><td>keywords</td><td>www, browser, internet</td></tr>
<tr><td>author</td><td>Mikulas Patocka, Karel 'Clock' Kulhavy, Petr 'Brain' Kulhavy, Martin 'PerM' Pergel</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mikulas Patocka &lt;mikulas _at_ artax.karlin.mff.cuni.cz&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://links.twibright.com/download/</td></tr>
<tr><td>original&nbsp;site</td><td>http://links.twibright.com/download/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
